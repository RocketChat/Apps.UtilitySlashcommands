import { InviteAllFromCommand } from './InviteAllFromCommand';
import { InviteAllToCommand } from './InviteAllToCommand';
import { InviteCommand } from './InviteCommand';

// tslint:disable-next-line:variable-name
const invite_commands = [
    InviteAllToCommand,
    InviteAllFromCommand,
    InviteCommand,
];

export default invite_commands;
