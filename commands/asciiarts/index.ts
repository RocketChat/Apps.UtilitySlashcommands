import { GimmeCommand } from './GimmeCommand';
import { LennyfaceCommand } from './LennyfaceCommand';
import { ShrugCommand } from './ShrugCommand';
import { TableflipCommand } from './TableflipCommand';
import { UnflipCommand } from './UnflipCommand';

// tslint:disable-next-line:variable-name
const ascii_commands = [
    UnflipCommand,
    TableflipCommand,
    ShrugCommand,
    LennyfaceCommand,
    GimmeCommand,
];

export default ascii_commands;
